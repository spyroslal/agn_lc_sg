__author__ = 'spyroslalos'

import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

class AgnLightCurve(object):

    """AGN light curve class.

    Class representing a AGN light curve containing data
    associated with emmanoulopoulos lightcurves.

    input:
        time (array)    - Times of a lightcurve
        flux (array)    - Fluxes of a lightcurve
        tbin (int)      - Time bin length of a lightcurve

    """

    def __init__(self, time, flux, tbin):

        self.time = time
        self.flux = flux
        self.tbin = tbin
        self.periodogram = None
        self.pdf = None

    def lc_plot(self):

        """ Plot the light curve.

        :return:

        """

        plt.scatter(self.time, self.flux)
        plt.title("AGN Lightcurve")

        plt.show()

    def lc_plot_from_file(self, fname):

        f = open(fname, 'r')
        entries = zip(*[line.split(" ") for line in f.readlines()[1:]])

        plt.scatter(entries[0], entries[1])
        plt.title("AGN lightcurve")
        plt.show()
  
    def export_light_curve(self, fname, simulation_number, iteration_num = None, dataformat='%.10e'):

        """Save lightcurve (time, flux) to output text file.

        :param filename: name of output file
        :param dataformat:
        :return:
        """
        data = np.array([self.time,self.flux]).T

        if iteration_num is not None:
            header_ttl = "\nSimulation number {} -- Iteration number {}\nTime\tFlux".format(simulation_number, iteration_num)
        else:
            header_ttl = "\nSimulation number {}\nTime\tFlux".format(simulation_number)
        with open(fname,'a+') as f_handle:
            np.savetxt(f_handle, data, fmt = dataformat, header=header_ttl)
