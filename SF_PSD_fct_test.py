import numpy as np

def brokenPL_PSD(v,fbr,A1,A2,alpha1,alpha2):
    """
    Broken power law PSD

    INPUTS
    :param v (array)                frequencies
    :param A,A2 (float)             normalisations
    :param alpha1, alpha2 (float)   exponents

    OUTPUT
    out                             output powers
    """

    out = np.zeros(len(v))

    out[v <= fbr] = A1 * (v[v <= fbr] ** alpha1)
    out[v > fbr] = A2 * (v[v > fbr] ** alpha2)

    return out