"""
Tensorflow && Python version of light curve generation simulation
algorithm by Emmanoulopoulos.

The algorithm is based on the mathetica implementation
provided by Emmanoulopoulos.

"""
import tensorflow as tf
import numpy as np
from light_curve import AgnLightCurve
import argparse
from argparse import RawTextHelpFormatter
from configobj import ConfigObj
import scipy.stats as stats
from helpers import emman_loop
from helpers import timmer_koenig, psd_model_gen, pdf_model_gen
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import timeit
import helpers as hlp
import SF_PSD_fct_test as spft
import matplotlib


def PSD_from_LC(LC, tbin, imax):
    """
    INPUT
    :param LC       light curve
    :param tbin     tbin
    :param imax     maximum index to consider for the periodogram
                    should be 0.5 * length + 1, but may change

    OUTPUT
    :out per_val    periodogram
    :out per_freq   frequencies for the periodogram
    """

    # discrete Fourier transform
    fft_LC = np.fft.fft(LC)
    freq_fft = np.fft.fftfreq(LC.shape[-1])

    # get freauencies
    # consider only positive j
    # last entry: multiply freq by -1 and take complex con. of the DFT (cc not needed since we take the modulus)
    per_freq_all = freq_fft[:imax] / tbin
    per_freq_all[-1] *= -1.

    # get periodogram
    mean_LC = np.mean(LC)
    length = len(LC)
    fact_per = 2. * tbin / (length * mean_LC ** 2.)
    per_val_all = fact_per * np.abs(fft_LC[:imax]) ** 2.

    # output
    # I do not consider j = 0

    return per_freq_all[1:], per_val_all[1:]

def set_up_argparse():

    parser = argparse.ArgumentParser(description = """This is a Tensorflow-Python implementation of light curve generation simulation algorithm by Emmanoulopoulos.
    \nExample:  python emman_train.py -c config.ini
    """, formatter_class=RawTextHelpFormatter)
    parser.add_argument('--config_file', '-c',  required=True, help = 'Configuration file path')
    args = parser.parse_args()

    return args

def get_scipy_stat_method(name):
    return getattr(stats, name, None)

def get_custom_stat_method(name):
    return getattr(hlp, name, None)

def main(args):

    cfg_file_name = args.config_file
    try:
        config = ConfigObj(cfg_file_name)
    except IOError:
        print "Error: Config file {} does not appear to exist.".format(cfg_file_name)
        return 0

    # Parameters initialization based on config file
    custom_pdf = None
    psd_params = map(float, config['psd'].values())
    pdf_params = config['pdf'].values()
    custom_discrete_values = int(pdf_params[0])
    custom_logspace = pdf_params[1]
    pdf_params = pdf_params[2:]
    pdf_params = map(float, pdf_params)

    if custom_logspace == 'true':
        custom_logspace = True
    else:
        custom_logspace = False

    stat_dists = config['stat_functions'].values()
    if stat_dists[0] == 'true':
        stat_dists = map(get_custom_stat_method, stat_dists[1:])
        custom_pdf = True
    else:
        stat_dists = map(get_scipy_stat_method, stat_dists[1:])
        custom_pdf = False

    mean_norm, sigma_norm = float(config['other']['mean_for_norm']),\
                            float(config['other']['sigma_for_norm'])
    red_noise_length, alias_time_bin = int(config['other']['red_noise_length']),\
                                       int(config['other']['alias_time_bin'])
    time_bin = int(config['other']['time_bin'])
    lc_length, random_seed = int(config['other']['lc_length']), int(config['other']['random_seed'])
    simulation_num = int(config['other']['simulations'])
    output_file = config['other']['output_file_name']

    if custom_pdf:
        pdf_params.append(custom_discrete_values)
        pdf_params.append(custom_logspace)
        pdf_params.append(lc_length)

    stop_crit = config['other']['stop_criterion']
    if stop_crit == 'delta':
        stop_value = float(config['other']['delta'])
    elif stop_crit == 'max_iter':
        stop_value = int(config['other']['max_iterations'])
    else:
        raise ValueError('Not valid stop criterion. Valid choices: delta && max_iter')

    start = timeit.default_timer()

    periods = []
    # For testing reasons
    # randomSeeds = [x for x in xrange(370,380)];

    for i in range(simulation_num):
        # print "Random seed {}".format(randomSeeds[i])
        # Step i
        light_curve, short_freq, short_power, fft = timmer_koenig(psd_model_gen, psd_params,\
                                        random_seed, red_noise_length, lc_length, time_bin,
                alias_time_bin, sigma_norm, mean_norm)

        time = np.arange(0,lc_length*time_bin, time_bin)

        # Step ii, iii, iv, v
        with tf.Session() as sess:
            surrogate, psd_last, periodogram, ffti = emman_loop(pdf_model_gen, pdf_params, fft.eval(), time,\
                short_freq.eval(), short_power.eval(), mean_norm, sigma_norm, lc_length,\
                    time_bin, stop_crit, stop_value, stat_dists, simulation_num, output_file, i,\
                        custompdf = custom_pdf, customlogsp = custom_logspace)
        tf.reset_default_graph()

        stop = timeit.default_timer()
        print "Simulation time: {}".format(stop - start)

        lc = AgnLightCurve(surrogate[0], surrogate[1], time_bin)
        lc.periodogram = psd_last
        # lc.lc_plot()
        # lc.Plot_Periodogram()
        periods.append(psd_last)

        # Plot Light Curve PDF in each iteration
        #
        # plt.hist(lc.flux, bins='auto', log=True)
        # plt.show()

    # #############################################################################
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Plot PSDs for each iteration and save them in file
    # Works for 10 simulations
    # //TODO: Arbitrary number of simulations figure

    # plt.hist(lc.flux, bins='auto', log=True)
    # plt.show()

    # Define the input PSD to be represented as a red line in our plot
    # %%%%%%%
    fmin_test1 = 5e-8
    fmax_test1 = 1.e-3
    v_PSD_Test = np.logspace(np.log10(fmin_test1), np.log10(fmax_test1), 1000)
    psd_test = spft.brokenPL_PSD(v_PSD_Test, psd_params[0], psd_params[1],\
                                 psd_params[2], psd_params[3], psd_params[4])
    # %%%%%%%
    # End of PSD Example 1

    # xx_low = np.linspace(5e-8, 2.e-5, 1000)
    # yy_low = 5.e-18 * (xx_low ** (-2.8))
    #
    # xx_high = np.linspace(2.e-5, 1.e-3, 1000)
    # yy_high = 1.e-10 * (xx_high ** (-1.2))
    #
    # num_column = 5
    # num_rows = 2
    #
    # fig = plt.figure(figsize=(30,10))
    #
    # gs = gridspec.GridSpec(num_rows, num_column)
    #
    # for i in xrange(len(periods)):
    #
    #     row_here = (i - i % num_column) / num_column
    #     col_here = i % num_column
    #
    #     pdgram = periods[i]
    #
    #     p=plt.subplot(gs[row_here,col_here])
    #
    #     plt.scatter(pdgram[0], pdgram[1])
    #     plt.plot(v_PSD_Test, psd_test, marker = '', markersize = 1, linestyle = '--', color = 'gray',\
    #              linewidth = 1.5, rasterized=True)
    #     plt.title("Simulation {}".format(i))
    #     plt.xlabel(r'$\nu$ [Hz]', size=10)
    #     plt.ylabel(r'PSD', size=10)
    #     p.set_yscale('log')
    #     p.set_xscale('log')
    #     plt.xlim([0.5*min(pdgram[0]),1.5*max(pdgram[0])])
    #     plt.ylim([0.5*min(pdgram[1]),1.5*max(pdgram[1])])
    #     plt.subplots_adjust(left=0.05,right=0.95,top=0.95,bottom=0.05,
    #                         hspace=0.3,wspace=0.3)
    #
    # out_psd_fname = str(simulation_num) + '_' + str(lc_length)
    # plt.savefig(out_psd_fname)
    #
    # fmin_test1 = 5e-8
    # fmax_test1 = 1.e-3
    # v_PSD_Test = np.logspace(np.log10(fmin_test1), np.log10(fmax_test1), 1000)
    # psd_test = spft.brokenPL_PSD(v_PSD_Test, psd_params[0], psd_params[1],\
    #                              psd_params[2], psd_params[3], psd_params[4])
    # # %%%%%%%
    # # End of PSD Example 1
    #
    # pdgram = psd_last
    # fig = plt.figure(figsize=(10,6))
    # p=plt.subplot(111)
    #
    # plt.scatter(pdgram[0], pdgram[1])
    # plt.plot(v_PSD_Test, psd_test, marker = '', markersize = 1, linestyle = '--', color = 'gray',\
    #          linewidth = 1.5, rasterized=True)
    # plt.title("Simulation {}".format(i))
    # plt.xlabel(r'$\nu$ [Hz]', size=10)
    # plt.ylabel(r'PSD', size=10)
    # p.set_yscale('log')
    # p.set_xscale('log')
    # plt.xlim([0.5*min(pdgram[0]),1.5*max(pdgram[0])])
    # plt.ylim([0.5*min(pdgram[1]),1.5*max(pdgram[1])])
    # plt.subplots_adjust(left=0.05,right=0.95,top=0.95,bottom=0.05,
    #                     hspace=0.3,wspace=0.3)
    #
    # out_psd_fname = str(simulation_num) + '_' + str(lc_length)
    # plt.savefig(out_psd_fname)

if __name__ == "__main__":
    args = set_up_argparse()
    main(args)


