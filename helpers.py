from light_curve import AgnLightCurve

__author__ = 'spyroslalos'

import numpy as np
import tensorflow as tf
import numpy.fft as ft
import numpy.random as rnd
import scipy.stats as st
from scipy import stats
import scipy.fftpack as scipyfft

from numpy import array, exp, pi, arange, concatenate


def ceilpow2(N):


    p = 1
    while p < N:
        p *= 2
    return p

def fftbs(x):
    '''
        >>> data = [1, 2, 5, 2, 5, 2, 3]
        >>> from numpy.fft import fft
        >>> from numpy import allclose
        >>> from numpy.random import randn
        >>> for n in range(1, 1000):
        ...     data = randn(n)
        ...     assert allclose(fft(data), fftbs(data))
    '''
    N = len(x)
    x = array(x)

    n = arange(N)
    b = exp((-1j*pi*n**2)/N)
    a = x * b.conjugate()

    M = ceilpow2(N) * 2
    A = concatenate((a, [0] * (M - N)))
    B = concatenate((b, [0] * (M - 2*N + 1), b[:0:-1]))
    C = ft.ifft(ft.fft(A) * ft.fft(B))
    c = C[:N]

    return b.conjugate() * c


def next_greater_power_of_2(x):
    return 2**(x-1).bit_length()

def take_numpy_eq(tensor, range1, range2):

    """ Run the numpy take operation on tensors.

    No implementation of take present on Tensorflow.
    Indexing in tf is quite limited.

    :param tensor:
    :param range1:
    :param range2:
    :return:

    """

    with tf.Session() as sess:
        if type(range1) == tf.Tensor:
            range1 = range1.eval()
        if type(range2) == tf.Tensor:
            range2 = range2.eval()

        result = np.take(tensor.eval(),range(range1, range2))
        return result

def timmer_koenig(psd_model, psd_params, randomSeed, red_noise_length, lc_length,time_bin,\
                  alias_time_bin, mean_norm, sigma_norm):

    # rnd.seed(randomSeed)
    rnd.seed()
    short_frequencies = tf.constant(np.arange(1.0, (red_noise_length*lc_length)/2 + 1)/\
                         (red_noise_length*lc_length*time_bin*alias_time_bin), dtype=tf.float32)

    power_law = psd_model_gen(short_frequencies, *psd_params)
    #tf.set_random_seed(randomSeed)
    amplitude = tf.sqrt(tf.multiply(power_law,0.5))
    real_part, img_part = [tf.multiply(amplitude,
                    tf.constant(rnd.normal(0,1,((red_noise_length*lc_length)/2)), dtype=tf.float32)) for i in range(2)]

    positive_freq = tf.complex(real_part, img_part)
    power_law_n, steps = artifial_lc_helper(positive_freq)

    inverse_fourier = fftbs(power_law_n)

    artificial_lc_all = tf.constant(inverse_fourier.real, dtype=tf.float64)

    random_subset_pointer = rnd.randint(lc_length-1, red_noise_length*lc_length -lc_length)

    light_curve = tf.constant(take_numpy_eq(artificial_lc_all, random_subset_pointer,\
                                    tf.add(random_subset_pointer, lc_length)))

    light_curve_pdf_mean, light_curve_variance = tf.nn.moments(light_curve,axes=[0])
    light_curve = tf.add(tf.multiply(tf.div(tf.subtract(light_curve, light_curve_pdf_mean),\
                                    tf.sqrt(light_curve_variance) ), sigma_norm ), mean_norm)

    light_curve_pdf_mean, light_curve_variance = tf.nn.moments(light_curve,axes=[0])
    light_curve_pdf_mean = tf.cast(light_curve_pdf_mean, dtype=tf.float32)

    with tf.Session() as sess:
        light_curve = light_curve.eval()
    fft = scipyfft.fft(light_curve)
    fft = tf.constant(fft)

    periodogram =  tf.multiply(tf.div( tf.multiply(tf.multiply(2.0, tf.to_float(time_bin)),\
                                    tf.to_float(alias_time_bin)), tf.multiply(tf.to_float(lc_length),
                                        tf.pow(light_curve_pdf_mean, 2) )), tf.cast(tf.pow(tf.abs(fft), 2), dtype=tf.float32))

    short_power = tf.constant(take_numpy_eq(periodogram, 1, tf.add(tf.div(lc_length, 2), 1) ))
    short_freq = tf.multiply(tf.constant(take_numpy_eq(short_frequencies, 1, tf.add(tf.div(lc_length, 2), 1))),
                        red_noise_length)

    return light_curve, short_freq, short_power, fft

"""
Define PSD models (uncomment needed one)
"""

# Bended PL
# def psd_model_gen(v, alpha, fbend, alow, ahigh, c):
#
#     """PSD model generator given input array v.
#
#     :param v: (array) input variables
#     :param alpha: (float) normalization flot
#     :param fbend: (float) bending frequency
#     :param alow: (float) low frequency
#     :param ahigh: (float) high frequency
#     :param c: (float) constant offset
#     :return: array of powers
#
#     """
#
#     numerator = tf.pow(v, tf.multiply(-1.0,alow))
#     denominator = tf.add(1.0, tf.pow(tf.div(v,fbend), tf.subtract(ahigh, alow)))
#     out = tf.multiply(alpha, tf.div(numerator,denominator))
#
#     return tf.add(out, c)

# Simple PL
#def psd_model_gen(v,psd_norm,alpha):
#    """
#    :param v: (array) input variables
#    :param psd_norm: (float) PSD normalisation factor
#    :param alpha: (float) power law exponent
#    """

#    out = tf.pow(v, alpha)

#    return tf.multiply(psd_norm, out)


# Broken PL
def psd_model_gen(v,fbr,A1,A2,alpha1,alpha2):
    """
    Broken power law PSD

    INPUTS
    :param v (array)                frequencies
    :param fbr (floa)               break frequency
    :param A,A2 (float)             normalisations
    :param alpha1, alpha2 (float)   exponents

    OUTPUT
    out                             output powers
    """
    with tf.Session() as sess:
        v = v.eval()
    out = np.zeros(len(v))
    out[v <= fbr] = A1 * (v[v <= fbr] ** alpha1)
    out[v > fbr] = A2 * (v[v > fbr] ** alpha2)

    return tf.constant(out, dtype=tf.float32)


def custom_gaussian(x, scale, mean, sigma):
    return scale * np.exp(-((x - mean)**2 / (2 * sigma**2)))

def custom_pdf_logormal(x, l_star, sigma):
    return np.exp( -(( np.log10(x) - np.log10(l_star))**2.)/(2.*(sigma**2)) )


def custom_pdf_bpl(x, l_star, d1, d2):
    return 1./((x/l_star)**d1 + (x/l_star)**d2)


def custom_pdf_gaussian_gen(lower_limit, upper_limit, mean, sigma,\
                            NumOfDisValues, logspace, dist_len):
    """
    Generate time series following custom PDF
    for Gaussian normal distribution
    :param lower_limit:
    :param upper_limit:
    :param mean:
    :param sigma:
    :param NumOfDisValues:
    :param logspace:
    :param dist_len:
    :return:
    """
    u_dist = np.linspace(lower_limit, upper_limit, NumOfDisValues)
    # lower_limit, upper_limit = u_dist[u_dist.size/2], u_dist[u_dist.size-1]

    gauss_res = [custom_gaussian(x, 1., mean, sigma) for x in u_dist]

    gauss_res = np.array(gauss_res)/sum(gauss_res)
    return np.random.choice(u_dist, dist_len, p=gauss_res)


def custom_pdf_bpl_gen(lower_limit, upper_limit, l_star, d1, d2,\
                       NumOfDisValues, logspace, dist_len):
# def custom_pdf_bpl_gen(params):
    """
    Generate time series following custom PDF
    for Broken Power Law distribution

    :param lower_limit:
    :param upper_limit:
    :param l_star:
    :param d1:
    :param d2:
    :param NumOfDisValues:
    :param logspace:
    :param dist_len:
    :return:
    """
    # lower_limit, upper_limit, l_star = params[0], params[1], params[2]
    # d1, d2, logspace = params[3], params[4], params[5]
    u_dist = np.linspace(lower_limit, upper_limit, NumOfDisValues)
    # u_dist = np.arange(lower_limit, upper_limit, 0.01)
    u_dist_original = u_dist
    if logspace:
        u_dist = 10.**u_dist
        l_star = 10.**(l_star)

    u_dist = u_dist.tolist()
    gauss_res = []
    for i in u_dist:
        gauss_res.append(custom_pdf_bpl(i, l_star, d1, d2))

    gauss_sum = sum(gauss_res)
    # normalization
    gauss_res = np.array(gauss_res)/gauss_sum
    gauss_res = np.random.choice(u_dist_original, dist_len, p=gauss_res)
    return gauss_res


def custom_pdf_lognormal_gen(lower_limit, upper_limit, l_star, sigma, NumOfDisValues, logspace, dist_len):
# def custom_pdf_lognormal_gen(params, length=0):
    """
    Generate time series following custom PDF
    for LogNormal distribution

    :param lower_limit:
    :param upper_limit:
    :param l_star:
    :param sigma:
    :param NumOfDisValues:
    :param logspace:
    :param dist_len:
    :return:
    """
    # lower_limit, upper_limit, l_star = params[0], params[1], params[2]
    # sigma, logspace = params[3], params[4]

    # lower_limit, upper_limit = -8, 0
    u_dist = np.linspace(lower_limit, upper_limit, NumOfDisValues)
    if logspace:
        u_dist = 10.**u_dist
        l_star = 10.**(l_star)
    # u_dist = np.arange(lower_limit, upper_limit, 0.01)
    u_dist_original = u_dist
    u_dist = u_dist.tolist()

    gauss_res = []
    for i in u_dist:
        gauss_res.append(custom_pdf_logormal(i, l_star, sigma))

    original_gauss_res = gauss_res
    gauss_sum = sum(gauss_res)

    # normalization
    gauss_res = np.array(gauss_res)/gauss_sum
    gauss_res = np.random.choice(u_dist_original, dist_len, p=gauss_res)

    return gauss_res

"""
 Returns random sampling result
 of PDF distribution
"""
def pdf_model_gen(pdf_params, stat_dists, custom_logspace, length=1, custom_pdf = False):


    mix_model_pdf = PDFDist(stat_dists, custom_pdf, custom_logspace)
    model_dist = mix_model_pdf.Sample(pdf_params, length)

    return model_dist

def artifial_lc_helper(complex_list):

    with tf.Session() as sess:
        positive_freq = complex_list.eval()
        power_law_n = np.append(positive_freq, positive_freq.conjugate()[::-1])
        power_law_n_2 = np.insert(power_law_n, 0, complex(0.0,0.0))

    return power_law_n_2, 0

def emman_loop(pdf_model_gen, pdf_params, fft, time, short_freq,\
        short_period, mean_norm, sigma_norm, lc_length, time_bin, stop_criterion,\
               stop_value, stat_dists, simulation_num, output_file, curr_sim, \
               custompdf = False, customlogsp = None ):

    model_dist = pdf_model_gen(pdf_params, stat_dists, customlogsp, \
                               length = lc_length, custom_pdf = custompdf)

    i = 0
    periodogram = [short_freq, short_period]
    sortdist = model_dist[np.argsort(model_dist)]
    surrogate_prev, surrogate = np.array([-1]), np.array([1])
    surrogate_deltas = []
    cur_delta = []

    # main iterations loop for convergence
    while np.array_equal(surrogate,surrogate_prev) == False:

        surrogate_prev = surrogate

        # First iteration
        if i == 0:
            # start with random distribution from PDF
            surrogate = [time, model_dist]
            surrogate_prev = [time, [0 for l in range(lc_length)]]
        else:
            surrogate = (adj_amp - np.mean(adj_amp)) / np.std(adj_amp)
            # renormalised LC
            surrogate = [time,(surrogate * sigma_norm) + mean_norm]

        ffti = ft.fft(surrogate[1])
        psd_k = ((2.0*time_bin)/(lc_length*(mean_norm**2))) *np.absolute(ffti)**2
        psd_k = [periodogram[0],np.take(psd_k,range(1,lc_length/2 +1))]
        fftAdj = np.absolute(fft)*(np.cos(np.angle(ffti)) + 1j*np.sin(np.angle(ffti)))

        adj_lc = ft.ifft(fftAdj)
        adj_lc = [time, ((adj_lc - np.mean(adj_lc))/np.std(adj_lc))* sigma_norm + mean_norm]
        adj_psd_lc = ((2*time_bin)/(lc_length*np.mean(adj_lc)**2.0))* np.absolute(ft.fft(adj_lc))**2
        adj_psd_lc = [periodogram[0],np.take(adj_psd_lc, range(1,lc_length/2 +1))]

        ranking = np.argsort(np.argsort(adj_lc[1]))
        adj_amp = sortdist[ranking]

        cur_delta = abs(surrogate[1]-surrogate_prev[1])
        # print 'Iteration {}: Surrogate delta is {}'.format(i, cur_delta)
        surrogate_deltas.append(cur_delta)

        # simulation==1, save results of all iterations on a single file
        if simulation_num==1:
            lc = AgnLightCurve(surrogate[0], surrogate[1], time_bin)
            lc.export_light_curve(output_file, curr_sim+1, iteration_num=i+1)

        # reached max iterations or desired delta depending on run mode
        if (stop_criterion == 'max_iter' and i == stop_value-1)\
                or ( stop_criterion == 'delta' and all(j<stop_value for j in cur_delta)):
            break

        i+=1

    # if simulations>1, save only the result of the last iteration
    if simulation_num>1:
            lc = AgnLightCurve(surrogate[0], surrogate[1], time_bin)
            lc.export_light_curve(output_file, curr_sim+1)

    return surrogate, psd_k, periodogram, ffti


"""
Based on existing implementations available on the web

"""
class PDFDist(object):

    def __init__(self, functions, custompdf, custom_log_space):
        self.functions = functions
        self.custom_pdf = custompdf
        self.logspace = custom_log_space
        self.__name__ = 'PDFDist'

    def Sample(self, params, length=1):

        # 1) create args array starting from params
        #    look at https://docs.scipy.org/doc/scipy-0.19.0/reference/stats.html for details about scipy.stats functions
        #    NOTE: below I give two example, uncomment only the needed one (or add for different distributions)

        # EXAMPLE: mixture distribution with Gamma and Lognorm
        #    gamma: a = kappa, loc = 0, scalse = theta
        #    lognorm: s = sigma, loc = 0, scale = exp(mu)
        #    pdf_params = [kappa, theta, pdf_sigma, pdf_mean, weight, 1-weight]

        # args = [[params[0], 0, params[1]], [params[2], 0, np.exp(params[-3])]]

        # EXAMPLE: normal distribution
        #    norm: loc = mu, scale = sigma
        #    pdf_params = [mu,sigma]

        # args = [params[0], params[1]]


        # 2) create sample using .rvs
        #    NOTE: here I give only the case with len(self.functions) = 1,2. Other cases may be added later.
        # EXAMPLE: normal distribution
        if (len(self.functions) == 1):

            # Custom pdf function will be used
            if self.custom_pdf:
                sample = self.functions[0](*params)
            # Built-in scipy function will be used
            else:
                args = [params[0], params[1]]
                sample = self.functions[0].rvs(loc = args[0], scale = args[1], size = length)

        # EXAMPLE: mixture distribution with Gamma and Lognorm
        if (len(self.functions) == 2):

            args = [[params[0], 0, params[1]], [params[2], 0, np.exp(params[-3])]]

            weight_cum = np.cumsum(params[-len(self.functions):])
            # random sample for function choice
            mix = np.random.random(size=length)*weight_cum[-1]
            sample = np.array([])

            for i in range(len(self.functions)):

                if i > 0:

                    sample = np.append(sample, self.functions[i].rvs(args[i][0], scale=args[i][2],
                                    loc=args[i][1], size=len(np.where((mix>weight_cum[i-1])*
                                                    (mix<=weight_cum[i]))[0])))
                else:
                    sample = np.append(sample, self.functions[i].rvs(args[i][0], scale=args[i][2],
                                 loc=args[i][1], size=len(np.where((mix<=weight_cum[i]))[0])))

            np.random.shuffle(sample)



        if len(sample) == 1:
            sample = sample[0]

        return sample


